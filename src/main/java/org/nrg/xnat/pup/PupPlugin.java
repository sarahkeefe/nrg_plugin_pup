package org.nrg.xnat.pup;

import org.nrg.framework.annotations.XnatDataModel;
import org.nrg.framework.annotations.XnatPlugin;
import org.springframework.context.annotation.ComponentScan;

@XnatPlugin(value = "nrg_plugin_pup", name = "XNAT 1.7 PUP Plugin", description = "This is the XNAT 1.7 PUP Plugin.",
        dataModels = {@XnatDataModel(value = "pup:pupTimeCourseData",
                singular = "PUP Timecourse",
                plural = "PUP Timecourses")})
@ComponentScan({"org.nrg.xnat.workflow.listeners"})
public class PupPlugin {
}